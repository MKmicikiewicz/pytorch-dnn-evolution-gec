from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name='torch-dnnevo',
    version='0.1',
    description='DNN evolution framework for pytorch',
    author='Pawel Koperek',
    author_email='pkoperek@gmail.com',
    url='https://gitlab.com/pkoperek/pytorch-dnn-evolution',
    packages=find_packages(include=['torchx', 'torchx.*']),
    long_description=long_description,
    install_requires=[
        'torch',
        'torchvision',
        'deap',
        'numpy',
    ],
)
