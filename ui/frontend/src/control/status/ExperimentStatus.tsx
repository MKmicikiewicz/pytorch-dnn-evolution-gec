import * as React from 'react';
import '../Control.css';
import {Button, Grid, Row,} from 'react-bootstrap';

interface ExperimentStatusProps {
    codeVersion: string,
    status: string,
    name: string,
    active: boolean,
    stop: () => void,
}

export const ExperimentStatus: React.StatelessComponent<ExperimentStatusProps> = (props) => {
    return (
        <Grid className="ExperimentStatus-container">
            <Row><h3> Current Experiment </h3></Row>
            <Row><strong>Code Version:</strong> {props.codeVersion}</Row>
            <Row><strong>Status:</strong> {props.status}</Row>
            <Row><strong>Name:</strong> {props.name}</Row>
            <Row className="Experiment-button-container">
                <Button bsSize="small" bsStyle="danger" className="ExperimentStatus-button" disabled={!props.active}
                        onClick={props.stop}>
                    STOP
                </Button>
            </Row>
            <hr className="ExperimentStatus-divider"/>
        </Grid>
    );
};
