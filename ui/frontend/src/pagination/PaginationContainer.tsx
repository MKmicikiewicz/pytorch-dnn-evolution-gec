import * as React from 'react';
import {PaginationComponent} from "./PaginationComponent";
import {Subject} from "rxjs";

export default class PaginationContainer extends React.Component<any, any> {
    public constructor(props) {
        super(props);
        this.state = {
            pageCount: props.pageCount,
            pageNumber: props.currentPageNumber,
            pageNumberSubject: new Subject()
        };
        this.state.pageNumberSubject.asObservable().subscribe((newPage) => {
            this.props.handler(newPage);
        });
    }

    public render() {
        return (<PaginationComponent
            pageNumberSubject={this.state.pageNumberSubject}
            pageCount={this.props.pageCount}
            pageNumber={this.props.currentPageNumber}/>);
    }

}