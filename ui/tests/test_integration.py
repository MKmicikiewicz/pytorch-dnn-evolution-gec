import os
import time
import unittest

import kubernetes as k8s
from socketIO_client import SocketIO, LoggingNamespace

NAMESPACE = os.environ.get('NAMESPACE', 'dnnevo')
DEPLOYMENT = 'dnnevo-driver-deployment'

k8s.config.load_kube_config()

test_exp_config = {
    'evo-config': "{\"fp_crossover_probability\":\"0.1\",\"fp_individual_size\":\"8\",\"fp_mutation_probability\":\"0.5\",\"fp_population_size\":\"4\",\"main_crossover_probability\":\"0.1\",\"main_individual_size\":\"8\",\"main_mutation_probability\":\"0.1\",\"main_population_size\":\"4\",\"stale_iterations_cnt\":\"1\",\"fp_train_config\":{\"batch_size\":\"128\",\"convolution_size_multiplier\":\"2\",\"dataset\":\"MNIST\",\"dense_size_multiplier\":\"1\",\"learning_rate\":\"0.1\",\"momentum\":\"0.9\",\"optimizer\":\"adam\",\"rng_seed\":\"0\",\"train_iterations\":\"1\"},\"main_train_config\":{\"batch_size\":\"128\",\"convolution_size_multiplier\":\"2\",\"dataset\":\"MNIST\",\"dense_size_multiplier\":\"1\",\"learning_rate\":\"0.1\",\"momentum\":\"0.9\",\"optimizer\":\"adam\",\"rng_seed\":\"1\",\"train_iterations\":\"1\"}}",
    'experiment-name': 'test',
    'max-iterations': '10'
}


class TestDNNEvoService(unittest.TestCase):

    def setUp(self):
        self.k8s_client_core = k8s.client.CoreV1Api()
        self.k8s_client_apps = k8s.client.AppsV1Api()
        self.socketIO_client = SocketIO('localhost', 8080, LoggingNamespace)
        self.saved_experiment_id = {'id': 1}
        self.received_status = False
        self.received_list = False

    # helper functions
    def receive_state_update(self, expected: str):
        def receive(received):
            print(f'Received status message: {received}')
            self.received_status = True
            self.assertEqual(received['status'], expected)

        return receive

    def receive_experiment_list(self, expected: list):
        def receive(received):
            print(f'Received experiment list message: {received}')
            self.received_list = True
            if len(received) > 0:
                self.saved_experiment_id = {'id': received[0]['id']}
            self.assertEqual(received, expected)

        return receive

    def get_driver_pods(self) -> list:
        dnnevo_pods = self.k8s_client_core.list_namespaced_pod(namespace=NAMESPACE, watch=False)
        return list(filter(lambda p: 'driver' in p.metadata.name, dnnevo_pods.items))

    def check_pod_state(self, pods: list):
        running = True
        for pod in pods:
            running = running or pod.status.phase == 'Running'
        self.assertTrue(running)

    def restart_one_pod(self, expected_state: str):
        expected_number = 2
        # test
        # check number of pods
        driver_pods = self.get_driver_pods()
        self.assertEqual(len(driver_pods), expected_number)
        self.check_pod_state(driver_pods)
        # kill one pod
        self.k8s_client_core.delete_namespaced_pod(name=driver_pods[0].metadata.name, namespace=NAMESPACE)
        time.sleep(30.0)
        # check connection
        self.received_status = False
        self.socketIO_client = SocketIO('localhost', 8080, LoggingNamespace)
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.assertTrue(self.socketIO_client.connected)
        self.socketIO_client.wait(10.0)
        # check number of pods
        driver_pods = self.get_driver_pods()
        self.assertEqual(len(driver_pods), expected_number)
        self.check_pod_state(driver_pods)
        self.assertTrue(self.received_status)

    def scale_deployment(self, change: int, expected_state: str):
        # test
        # check number of pods
        driver_pods = self.get_driver_pods()
        number_before = len(driver_pods)
        number_after = number_before + change
        self.check_pod_state(driver_pods)
        # scale up deployment
        body = {'spec': {'replicas': number_after}}
        self.k8s_client_apps.patch_namespaced_deployment(name=DEPLOYMENT, namespace=NAMESPACE, body=body)
        time.sleep(45.0)
        # check connection
        self.received_status = False
        self.socketIO_client = SocketIO('localhost', 8080, LoggingNamespace)
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.assertTrue(self.socketIO_client.connected)
        self.socketIO_client.wait(10.0)
        # check number of pods
        driver_pods = self.get_driver_pods()
        self.assertEqual(len(driver_pods), number_after)
        self.check_pod_state(driver_pods)
        self.assertTrue(self.received_status)

    # tests
    def test_01_check_pods(self):
        print(f'Checking pods in {NAMESPACE} namespace:')
        pods = self.k8s_client_core.list_namespaced_pod(namespace=NAMESPACE, watch=False).items
        for pod in pods:
            print(pod.metadata.name)
        self.assertEqual(len(pods), 6)
        self.check_pod_state(pods)

    def test_02_check_connection(self):
        print(f'Checking connection to dnnevo backend')
        expected_state = 'NOT_STARTED'
        self.received_status = False
        # test
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.assertTrue(self.socketIO_client.connected)
        self.socketIO_client.wait(5.0)
        self.assertTrue(self.received_status)

    def test_03_list_experiments_empty(self):
        print(f'Checking empty list of experiments')
        expected_list = []
        self.received_list = False
        # test
        self.socketIO_client.on('exp_list', self.receive_experiment_list(expected_list))
        self.socketIO_client.emit('exp_list_request')
        self.socketIO_client.wait(10.0)
        self.assertTrue(self.received_list)

    def test_04_resume_experiment_fail(self):
        print(f'Checking resuming without saved experiments')
        expected_state = 'NOT_STARTED'
        # test
        self.socketIO_client.emit('exp_resume', self.saved_experiment_id)
        self.received_status = False
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.socketIO_client.wait(5.0)
        self.assertTrue(self.received_status)

    def test_05_start_experiment(self):
        print(f'Checking starting experiment')
        expected_state = 'RUNNING'
        # test
        self.socketIO_client.emit('exp_start', test_exp_config)
        self.received_status = False
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.socketIO_client.wait(5.0)
        self.assertTrue(self.received_status)

    def test_06_list_experiments(self):
        print(f'Checking list of experiments')
        expected_list = [{
            'id': 1,
            'name': f"{test_exp_config['experiment-name']}",
            'max_iter': int(test_exp_config['max-iterations']),
            'iter_no': 0
        }]
        # test
        self.received_list = False
        self.socketIO_client.on('exp_list', self.receive_experiment_list(expected_list))
        self.socketIO_client.emit('exp_list_request')
        self.socketIO_client.wait(10.0)
        self.assertTrue(self.received_list)

    def test_07_kill_one_pod_failover(self):
        print(f'Checking failover on restarting pod (experiment running)')
        self.restart_one_pod(expected_state='RUNNING')

    def test_08_scale_up_failover(self):
        print(f'Checking failover on scaling up (experiment running)')
        self.scale_deployment(change=1, expected_state='RUNNING')

    def test_09_scale_down_failover(self):
        print(f'Checking failover on scaling down (experiment running)')
        self.scale_deployment(change=-1, expected_state='RUNNING')

    def test_10_stop_experiment(self):
        print(f'Checking stopping experiment')
        expected_state = 'NOT_STARTED'
        # test
        self.socketIO_client.emit('exp_stop')
        self.received_status = False
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.socketIO_client.wait(5.0)
        self.assertTrue(self.received_status)

    def test_11_kill_one_pod_stopped(self):
        print(f'Checking failover on restarting pod (experiment stopped)')
        self.restart_one_pod(expected_state='NOT_STARTED')

    def test_12_scale_up_stopped(self):
        print(f'Checking failover on scaling up (experiment stopped)')
        self.scale_deployment(change=1, expected_state='NOT_STARTED')

    def test_13_scale_down_stopped(self):
        print(f'Checking failover on scaling down (experiment stopped)')
        self.scale_deployment(change=-1, expected_state='NOT_STARTED')

    def test_14_resume_experiment(self):
        print(f'Checking resuming experiment')
        expected_state = 'RUNNING'
        # test
        self.socketIO_client.emit('exp_resume', self.saved_experiment_id)
        self.received_status = False
        self.socketIO_client.on('exp_status', self.receive_state_update(expected_state))
        self.socketIO_client.wait(5.0)
        self.assertTrue(self.received_status)


if __name__ == '__main__':
    import xmlrunner

    unittest.main(testRunner=xmlrunner.XMLTestRunner(outsuffix='integration'))
