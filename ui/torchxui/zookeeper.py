import logging
import os
from typing import Callable

from kazoo.client import KazooClient, KeeperState, KazooRetry
from kazoo.exceptions import KazooException

logger = logging.getLogger(__name__)
ENCODING = 'utf-8'


class DNNEvoZookeeperClient(KazooClient):
    experiment_id_path = '/dnnevo/experiment/id'
    leader_election_path = '/dnnevo/leader/election'

    def __init__(self):
        zk_server_address = os.environ.get('ZOOKEEPER_SERVER')
        kazoo_retry = KazooRetry(max_tries=5)
        super().__init__(hosts=zk_server_address, read_only=False, connection_retry=kazoo_retry)

        self.start()
        self.ensure_path(self.experiment_id_path)

        @self.add_listener
        def connection_listener(state: KeeperState):
            if state == KeeperState.CONNECTED:
                logger.info('ZK state is CONNECTED')
            else:
                # We already passed retry object to KazooClient
                logger.info('Connection lost')

    def elect_leader(self, instance_id: str, callback: Callable):
        """
        Blocks until the election is won, then calls callback().
        Listens to leader heartbeat all the time.
        """
        election = self.Election(self.leader_election_path, instance_id)
        logger.info('Client ' + instance_id + ' joined leader election')

        election.run(callback)

    def get_experiment_id(self) -> str:
        try:
            experiment_id, _ = self.retry(self.get, self.experiment_id_path)
            return experiment_id.decode(ENCODING)
        except KazooException:
            return ''

    def set_experiment_id(self, experiment_id: int):
        try:
            self.retry(self.set, self.experiment_id_path, str(experiment_id).encode(ENCODING))
        except KazooException:
            return
