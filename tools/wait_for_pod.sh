#!/usr/bin/env bash
KUBECTL=${KUBECTL:-$(which kubectl)}

function wait_for_pod() {
    local NAME=$1
    local NAMESPACE=$2
    local READY
    local TIME=0
    echo "Waiting for pod $NAME in namespace $NAMESPACE:"
    while [ "$READY" == "" ]; do
      echo "Not ready (waiting ${TIME}s)"
      sleep 20s
      READY=$(kubectl get pod "$NAME" -n "$NAMESPACE" --no-headers | grep Running)
      TIME=$((TIME + 20))
    done
    echo "Pod $NAME ready"
}

wait_for_pod "$@"